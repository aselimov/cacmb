# Install CACmb

In order to build CACmb switch to the src repository and run:

```
make
```

Once this command is complete you can run:

```
sudo make install
```

in order to copy the installation to `/usr/local/bin`.

