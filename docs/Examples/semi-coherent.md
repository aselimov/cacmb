# CuNi Bicrystal Interface
This example builds a bicrystal of CuNi with a semi-coherent interface. The interface region is rendered at full atomistic resolution and the bulk of the layers are coarse-grained.

```sh
#!/bin/bash

#Build Copper CG region and atom pad
cacmb --create Cu fcc 3.615 25 orient [-21-1] [011] [11-1] duplicate 7.44 6 4 Cu_cg.mb -ow 
cacmb --create Cu fcc 3.615 2 orient [-21-1] [011] [11-1] duplicate 93 75 3 Cu_atom.mb -ow
#Build Nickel CG region and atom pad
cacmb --create Ni fcc 3.52 25 orient [-21-1] [011] [11-1] duplicate 7.6409 6.16 4 Ni_cg.mb -ow 
cacmb --create Ni fcc 3.52 2 orient [-21-1] [011] [11-1] duplicate 95.511 77 3 Ni_atom.mb -ow
#Merge all into one block keeping full atomistic resolution at the interface
cacmb --merge z 4  Ni_cg.mb Ni_atom.mb Cu_atom.mb Cu_cg.mb -boundary pps cac_in.restart cac_in.xyz  -ow 

#Delete leftover files
rm Ni_cg.mb Ni_atom.mb Cu_atom.mb Cu_cg.mb 

```

Below are the .xyz file render which shows the atomistic and coarse-grained regions and the common neighbor analysi of the interface.

![xyz bicrystal render](../img/CuNi_xyz.png) 
![interface structure for bicrystal model](../img/CuNi_int.png) 
