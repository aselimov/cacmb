# Getting Started

The CACmb tool separates commands into two general groups, modes and options. 
Modes are the main commands which conduct primary operations, these are listed below:

* [Mode Convert](Modes/convert.md)
* [Mode Create](Modes/create.md) 
* [Mode Merge](Modes/merge.md) 

To start a cacmb run, it is required to run one of the available modes and only one mode can be run at a time.
Additionally one can use options in order to adjust the model during any of the mode runs and unlimited number of options may be used.
While single line commands may be used satisfactorily to build a model, in order to build more complex models it is required to run multiple commands.

Currently CACmb only supports rhombehedral FCC finite elements with faces aligned to {111} slip planes.
This finite element is the primitive unit cell for the FCC crystal and is shown below:

![Rhombohedral Element](img/rhomb.png) 

## Simple Example 1
For example, to build a model with an atomistic region bound by a coarse-grained region the following commands should be run:

```
cacmb --create Cu fcc 3.615 15 duplicate 4 2 1 orient [112] [1-10] [11-1] Cu_cg.mb -ow
cacmb --create Cu fcc 3.615 2 duplicate 30 15 2 orient [112] [1-10] [11-1] Cu_at.mb -ow
cacmb --merge z 3 Cu_cg.mb Cu_at.mb Cu_cg.mb model.xyz -ow
```

When visualizing the model.xyz file using ovito we see our desired model (where the red atoms represent the element nodes and blue atoms are atomistic regions):

![Simple example 1 render](img/simple_example_1.png) 

**Important Note: In order to fully utilize the cacmb capabilites use the .mb format when running multiple model build steps**

A description of each build step is below:

1. Build a coarse grained model with elements that have 15 atoms per edge. Orientation is set to `[112] [1-10] [11-1]`. We save it to `Cu_cg.mb` and pass `-ow` which is the flag to overwrite existing files with that name.
2. Build an atomistic model with the same dimensionsin the x and y directions as the coarse grained model. *A general way to match dimensions is by setting the duplicate value of the atoms to `esize*duplicate/2`. So for the x dimension we do `15*4/2` which is 30 and for the y dimension `15*2/2` which is 15.
3. Merge the models by stacking the coarse-grained and atomistic models. We start the stacking sequence from the bottom so the first region is the `Cu_cg.mb`, followed by the `Cu_at.mb` model in the middle, followed by another `Cu_cg.mb` region. We output it to model.xyz which renders the atoms and nodes of the model for visualization.

This whole process can be easily run as a shell script as long as cacmb is on your path.

```sh
#!/bin/sh
cacmb --create Cu fcc 3.615 15 duplicate 4 2 1 orient [112] [1-10] [11-1] Cu_cg.mb -ow
cacmb --create Cu fcc 3.615 2 duplicate 30 15 2 orient [112] [1-10] [11-1] Cu_at.mb -ow
cacmb --merge z 3 Cu_cg.mb Cu_at.mb Cu_cg.mb model.xyz -ow
```

