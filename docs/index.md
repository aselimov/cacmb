# CACmb: The Concurent Atomistic-Continuum Model Builder

CACmb is a tool used to build coarse-grained atomistic models for use with the PyCAC and LammpsCAC toolkits.
This tool also allows for the creation of equivalent atomistic models to be used in lammps throught the .lmp data format.

![cacmb image](img/demo.gif) 

## Requirements
CACmb requires only a fortran compiler and does not need any external libraries. 
This code has been tested using both gfortran and ifort fortran compilers solely on linux.

## Build instructions
CACmb can be built by the following commands:
```
git clone https://gitlab.com/aselimov/cacmb
cd cacmb/src
make
```
You can also install cacmb to /usr/local/bin by running:
```
sudo make install
```
Otherwise make sure your cacmb executable is available on your system path.

Please view the [Getting Started](Getting_Started.md) guide for an introduction and basic usage instructions.

## License
This tool is available under the MIT license which is printed below, a full copy of which is available at the cacmb [Gitlab repository.](https://gitlab.com/aselimov/cacmb/-/blob/development/LICENSE)



