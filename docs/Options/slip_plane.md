# Slip Plane
```
-slip_plane dim pos 
```

This command forces a slip plane at position `pos` along dimension `dim` by finding which elements that plane intersects and refining them to full atomistics.

## Optional Arguments 

### efill
```
-slip_plane dim pos efill
```
The efill option attempts to remesh the elements that are refined while ensuring an element discontinuity on the prescribed slip plane
