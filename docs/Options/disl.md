# Dislocation

There are various options for creating dislocations. 
These are listed below.

## dislgen
 
```
-dislgen [ijk] [hkl] x y z burgers char_angle poisson 
```

This options adds an arbitrarily oriented dislocation into your model based on user inputs using the volterra displacement fields. The options are below

`[ijk]` - The vector for the line direction

`[hkl]` - The vector for the slip plane 

`burgers` - The magnitude of the burgers vector for the dislocation to be inserted

`x y z` - The position of the dislocation centroid

`char_angle` - Character angle of the dislocation (0 is screw and 90 is edge)

`poisson` - Poisson's ratio used for the displacement field.

## disloop

````
-disloop loop_normal radius x y z bx by bz poisson
````

This option imposes the displacement field for a dislocation in order to create a loop. This loop is unstable and will close if stress isn't applied.

`loop_normal` - The box dimension which defines the normal to the loop plane. As of now this dimension must be a close packed direction, meaning that for fcc a box dimension has to be of the (111) family of planes. Either `x`, `y`, or `z`.

`radius` - The radius of the loop in Angstroms.

`x y z` - The centroid of the loop. 

`bx by bz` - The burgers vector for the dislocation

`poisson` - Poisson ratio for continuum solution

## vacancy disloop

```
-vacancydisloop loop_normal radius x y z 
```

This option creates a circular planar vacancy cluster of radius `radius` normal to the `loop_normal` centered on position `x y z`. Upon relaxing or energy minimization this cluster should become a prismatic dislocation loop.


