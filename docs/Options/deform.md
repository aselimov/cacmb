# Deform
```
-deform dim dl
```
This command is used to apply a uniaxial strain unto a simulation cell. 
The argument `dim` takes the values of `x, y, or z` and the argument `dl` is the change in length to be applied (in Angstroms.

