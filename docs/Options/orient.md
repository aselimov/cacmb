# Orient 

``` 
-orient [hkl] [hkl] [hkl]
```

This command transforms the simulation cell into the specified orientation. 
This can be useful when attempting to view the model along different slip planes.
In general the dimensions will not be periodic as the box will not have flat surfaces along the x, y, and z dimensions.
*Example:* `-orient [-112] [110] [-11-1]` 

This command also comes with a sub command `unorient`.
## Unorient
```
-unorient
```
This removes the orientation applied by an orient command during the current cacmb run.

