# Delete Overlap
 ```
-delete overlap rc_off
 ```
 
 This delete option removes overlapping atoms.
 For every neighboring pair of atoms, the distance between them is checked.
 If the distance is less than `rc_off` then the atom with the higher index in the data arrays is deleted.

