# Wrap
```
-wrap
```

This will wrap atomic positions back inside the box.
Effectively as if periodic boundary conditions are applied so that atoms which exit from one side of the simulation cell enter back in through the other.

