# Define Orientation

```
-sbox_ori sbox [hkl] [hkl] [hkl]
```

This option is primarily used when reading data from non .mb formats.
This code sets the orientation of the elements and atoms that are part of the sub_box `sbox` to the user provided orientation.
This doesn't change anything about the model, but is required in order to accurately use the `dislgen` or `orient` options when operating on non .mb files.

*Example:* 

```
cacmb --create Cu fcc 3.615 2 orient [112] [1-10] [11-1] duplicate 10 10 10 cac.lmp
cacmb --convert cac.lmp cac_out.lmp -sbox_ori 1 [112] [1-10] [11-1] -dislgen [112] [11-1] inf*0.5 inf*0.5 inf*0.5 2.556 90 0.3 
```
The above command attempts to insert a dislocation using the `dislgen` option. 
In order to do the coordinate transformation, the code needs to know what the orientation of the model originally and so the sbox_ori command sets the sbox information. 
To better understand how the sub_boxes work, please view sub_box in the Getting Started guide.

