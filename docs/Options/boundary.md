# Boundary

``` 
-boundary box_bc
```

This allows the user to specify the boundary conditions for the model being outputted. The format is a 3 character string with `p` indicating periodic and `s` indicating shrink-wrapped.

**Example** `ppp` - is a fully periodic model, `pss` is periodic in the x dimension and shrink wrapped in y and z.
