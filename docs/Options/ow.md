# Overwrite
```
-ow
```

If this option is passed then all files are automatically overwritten without asking the user.

