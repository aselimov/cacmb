# Group

```
-group select_type group_shape shape_arguments
```

This option selects a group of either elements or atoms and applies some transformation to them.

    `select_type` - Either `atoms`, `elements`, or 'all'. `elements` selects elements based on whether the element center is within the group. `all` selects elements based on the element center and atoms based on their position.

`group_shape` - Specifies what shape the group takes and dictates which options must be passed. Each shape requires different arguments and these arguments are represented by the placeholder `shape_arguments`. The accepted group shapes and arguments are below:

## Group Shapes
These are the allowed group shapes

### Block
```
-group atoms block xlo xhi ylo yhi zlo zhi
```

This selects a group residing in a block with edges perpendicular to the simulation cell. The block boundaries are given by `xlo xhi ylo yhi zlo zhi`. 

`additional keywords`- Represents the various transformations which can be performed on a group. These additional keywords are given below.

### Wedge
```
-group atoms wedge dim1 dim2 bx by bz bw
```

This selects a group which are within a wedge shape. The options are given as follows:
`dim1` - The dimension containing the plane normal of the wedge base.
`dim2` - The thickness dimension. Wedge groups are currently required to span the entire cell thickness in one dimensions which is normal to the triangular face. This through thickness dimension is dim2.
`bx by bz` - Centroid of the center of the base
`bw` - Base width

### Notch
```
-group atoms notch dim1 dim2 bx by bz bw tr
```

This shape is similar to a wedge shape except instead of becoming atomically sharp, it finishes in a rounded tip with tip radius `tr`. Options are as follows.
`dim1` - The dimension containing the plane normal of the wedge base.
`dim2` - The thickness dimension. Wedge groups are currently required to span the entire cell thickness in one dimensions which is normal to the triangular face. This through thickness dimension is dim2.
`bx by bz` - Centroid of the center of the base
`bw` - Base width
`tr` - Tip radius

### Sphere
```
-group atoms sphere x y z r
```

This shape selects all atoms within a sphere centered at `(x,y,z)` with radius `r`.

## Group selection operators

The following are a list of group operations which alter how the atoms/elements in the group are selected.


### Random
```
random n
```

This command selects `n` random atoms and `n` random elements within your group bounds.
If using group type `atoms` or `elements` then only `n` random atoms or elements are selected. 
These random atoms/elements then form the new group.

*Example:*
```
group atoms block -inf inf -inf inf inf*0.5 inf random 10
```
The above example selects 10 random atoms from the specified group.
These atoms will all have z values which place them in the top half of the cell.

### Node
```
nodes
```

This keyword changes the selection criteria when using `elements` or `both` to element nodes instead of element centroids.

### Flip

```
flip
```

This keyword changes the group selection criteria from the atoms/elements inside a region to the atoms/elements outside the group.

### Notsize
```
notsize esize
```
Notsize filters out elements of size `esize` when determining the group. For example, if the user runs `notsize 25` and the group bounds contain elements with esizes of 13, 15, 25 (from the efill command for example) then only the elements with esize 13 and 15 are selected as part of the group.


## Group modification operators

These modifiers operate on selected atoms and elements to adjust their properties.

### Displace 

```
displace x y z 
```
This operation shifts all grouped atoms/nodes by a vector `(x,y,z)`.

### Delete

```
delete
```
This command deletes all selected atoms and elements within the group.

### Type
```
type atom_type
```
This command changes all atoms and basis atoms at element nodes for the group elements and atoms to type `atom_type`. 
`atom-type` should be two characters which describe the atomic element (such as `Cu` or `Ni`)

### Refine
```
refine
```
This command refines all elements within the group to full atomistic resolution.


