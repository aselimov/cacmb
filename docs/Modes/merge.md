# Merge

```
cacmb --merge dim N infiles 
```

This mode merges multiple data files and creates one big simulation cell. The parameters are:

`dim` - the dimension they are to be stacked along, can be either `x`, `y`, or `z`. If the argument `none` is passed then the cells are just overlaid. 

`N` - The number of files which are being read

 `infiles` - The input files which are to be merged. There must be `N` of them.


## Additional options:

### Shift

```
shift x y z
```

If the shift command is passed to mode merge then each file after the first file in the merge command is displaced by the vector `[x, y, z]`. This is additive so if you are merging three files and this command is passed then the second file is shifted by `[x,y,z]` and the third file is shifted by `2*[x,y,z]`. 

Example: `cacmb --merge z 2 Cu.mb Cu2.mb Cu3.mb Cumerged.mb shift 2 0 0` will shift the atomic and element positions in the `Cu2.mb` file by `[2,0,0]` and the positions in `Cu3.mb` by `[4,0,0]`.


