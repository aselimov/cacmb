#Convert

```
cacmb --convert infile outfile(s)
```

This mode converts a file `infile` to different files `outfile(s)`. 
The extensions on both files determine which read and write subroutines are called.
This mode can also be used to apply options to a file by setting `infile` and `outfile` to the same value such as:
```
cacmb --convert cac.mb cac.mb {-options}
```

