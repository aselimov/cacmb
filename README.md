# CAC_Model_Builder
![](./cacmb.png)

This is a tool for building models in CAC. Commands and usage options are below. This code is intended to follow the atomsk code fairly closely.

Documentation is available at: [aselimov.gitlab.io/cacmb](https://aselimov.gitlab.io/cacmb)

A paper describing the use of this tool is currently in preparation.

