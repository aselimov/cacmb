module mode_convert

    use parameters
    use box
    use elements
    use io

    public 
    contains

    subroutine convert(arg_pos)
        !This subroutine converts a single input file from one format to another
        integer, intent(out) :: arg_pos
        character(len=100) :: infile
        real(kind = dp) :: temp_box_bd(6)
        !First read in the file
        temp_box_bd(:) = 0.0_dp
        call get_command_argument(2, infile)
        call get_in_file(infile)
        call read_in(1, (/0.0_dp,0.0_dp,0.0_dp/), temp_box_bd)
        call grow_box(temp_box_bd)
        arg_pos = 3

    end subroutine convert
end module mode_convert
