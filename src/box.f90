module box
    !This module contains information on the properties of the current box.
    use parameters
    use functions
    implicit none 

    real(kind=dp) :: box_bd(6) !Global box boundaries
    real(kind=dp) :: box_ori(3,3) !Box orientation
    character(len=3) :: box_bc !Box boundary conditions (periodic or shrinkwrapped)
    logical :: bound_called 
    !The subbox variables contain values for each subbox, being the boxes read in through some
    !command. 

    !Below are some simulation parameters which may be adjusted if reading in restart files
    integer :: timestep=0
    real(kind=dp) :: total_time=0.0_dp


    public 
    contains 

    subroutine box_init
        !Initialize some box functions
        box_bd(:) = 0.0_dp
        box_bc = 'ppp'
        bound_called=.false.
    end subroutine box_init

    subroutine grow_box(temp_box_bd)
        !This function takes in a temporary box boundary and adjusts the overall box boundaries
        !to include  it

        real(kind=dp), dimension(6), intent(in) :: temp_box_bd

        integer :: i

        if(all(abs(box_bd) < lim_zero)) then 
            box_bd = temp_box_bd
        else
            do i = 1, 3
                if(temp_box_bd(2*i-1) < box_bd(2*i-1)) box_bd(2*i-1) = temp_box_bd(2*i-1)
                if(temp_box_bd(2*i) > box_bd(2*i)) box_bd(2*i) = temp_box_bd(2*i)
            end do
        end if

        return
    end subroutine grow_box

    subroutine reset_box
        !This subroutine just resets the box boundary and position
        box_bc = "ppp"
        box_bd(:) = 0
    end subroutine reset_box

    pure function box_volume()
        real(kind = dp) :: box_volume

        box_volume = (box_bd(2) - box_bd(1)) * (box_bd(4) - box_bd(3))*(box_bd(6) - box_bd(5))
        return
    end function

end module box
