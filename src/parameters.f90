module parameters

    implicit none

    !Default precision
    integer, parameter :: dp= selected_real_kind(15,307), &
                          min_efillsize = 11
    !Parameters for floating point tolerance
    real(kind=dp), parameter :: lim_zero = epsilon(1.0_dp), &
                                lim_large = huge(1.0_dp), &
                                tol = 10.0_dp**(-6.0_dp)
    logical, save :: lmpcac 
    
    !Numeric constants
    real(kind=dp), parameter :: pi = 3.14159265358979323846_dp
    real(kind=dp), parameter :: pvt2n =  1362.626470955822
    
    !Mode type that is being called 
    character(len=100) :: mode
end module parameters
