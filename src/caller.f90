module caller
    !this module just calls modes and options

    use mode_da
    use mode_create
    use mode_convert
    use mode_merge
    use mode_metric
    use mode_calc
    use parameters
    use opt_disl
    use opt_group
    use opt_orient
    use opt_deform
    use opt_delete
    use opt_redef_box
    use opt_slip_plane
    use opt_bubble
    use box


    implicit none
    public 
    contains
    subroutine call_mode(arg_pos)
        !This code is used to parse the command line argument for the mode information and calls the required 
        !mode module.
    
        integer, intent(out) :: arg_pos
    
        select case(mode)
        case('--create')
            call create(arg_pos)
        case('--convert')
            call convert(arg_pos)  
        case('--merge')
            call merge(arg_pos) 
        case('--metric')
            call metric(arg_pos)
        case('--calc')
            call calc(arg_pos)
        case('--da')
            call da(arg_pos)
        case default
            print *, "Mode ", trim(adjustl(mode)), " currently not accepted. Please check documentation for ", &
                     "accepted modes and rerun."
    
            stop 3
        end select
    end subroutine call_mode

    subroutine call_option(option, arg_pos)
        integer, intent(inout) :: arg_pos
        character(len=100), intent(in) :: option
    
        select case(trim(adjustl(option)))
        case('-disl','-dislgen', '-disloop','-vacancydisloop')
            call dislocation(option, arg_pos)
        case('-group')
            call group(arg_pos)
        case('-ow')
            arg_pos = arg_pos + 1
        case('-novel')
            arg_pos = arg_pos+1
        case('-wrap')
            arg_pos = arg_pos + 1
        case('-norefine')
            arg_pos = arg_pos + 1
        case('-all_new')
            arg_pos = arg_pos + 1
        case('-orient')
            call orient_opt(arg_pos)
        case('-unorient')
            call unorient
            arg_pos = arg_pos + 1
        case('-boundary')
            arg_pos=arg_pos+1
            call get_command_argument(arg_pos, box_bc)
            arg_pos=arg_pos+1
            bound_called = .true.
        case('-box_ori')
            call set_box_ori(arg_pos)
        case('-deform')
            call deform(arg_pos)
        case('-delete')
            call run_delete(arg_pos)
        case('-set_cac')
            arg_pos=arg_pos +3
        case('-set_types')
            arg_pos = arg_pos + 3 + atom_types
        case('-redef_box')
            call redef_box(arg_pos)
        case('-slip_plane')
            call run_slip_plane(arg_pos)
        case('-bubble')
            call bubble(arg_pos)
        case default
            print *, 'Option ', trim(adjustl(option)), ' is not currently accepted.'
            stop 3 
        end select
    end subroutine call_option

end module caller
